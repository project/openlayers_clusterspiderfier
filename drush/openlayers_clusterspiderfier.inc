<?php
/**
 * @file
 * drush integration for Openlayers ClusterSpiderfier.
 */

/**
 * Implements hook_drush_command().
 */
function openlayers_clusterspiderfier_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['dl-clusterspiderfier'] = array(
    'callback' => 'drush_openlayers__plugin',
    'description' => dt('Download and install the Openlayers ClusterSpiderfier library.'),
    'bootstrap' => DRUSH_BOOTSTRAP_NONE,
    'arguments' => array(
      'path' => dt('Optional. A path where to install the ClusterSpiderfier library. If omitted Drush will use the default location.'),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function openlayers_clusterspiderfier_drush_help($section) {
  switch ($section) {
    case 'drush:dl-clusterspiderfier':
      return dt('Download and install the Openlayers ClusterSpiderfier library from https://github.com/CandoImage/OL3ClusterSpiderfier, default location is sites/all/libraries.');
  }
}

/**
 * Command to download the Openlayers ClusterSpiderfier library.
 */
function drush_openlayers_clusterspiderfier_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  $library = libraries_detect('OL3ClusterSpiderfier');

  // Download the archive.
  if ($filepath = drush_download_file($library['download url'])) {
    $filename = basename($filepath);
    $dirname = basename($filepath, '.zip');

    // Remove any existing Openlayers library directory.
    if (is_dir($dirname) || is_dir('OL3ClusterSpiderfier')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('OL3ClusterSpiderfier', TRUE);
      drush_log(dt('A existing OL3ClusterSpiderfier library was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the archive.
    drush_tarball_extract($filename);
    drush_move_dir($dirname, 'OL3ClusterSpiderfier', TRUE);
    $dirname = 'OL3ClusterSpiderfier';
  }

  if (is_dir($dirname)) {
    drush_log(dt('Openlayers ClusterSpiderfier library has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Openlayers ClusterSpiderfier library to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
