Drupal.openlayers.pluginManager.register({
  fs: 'openlayers.Interaction:ClusterSpiderfier',
  init: function(data) {
    var options = data.opt;
    // Ensure a proper style object is passed in.
    if (options.style) {
      if (typeof data.objects.styles[options.style] != 'undefined') {
        options.style = data.objects.styles[options.style];
      }
      else {
        delete options.style;
      }
    }
    return new ol.interaction.ClusterSpiderfier(options);
  }
});
