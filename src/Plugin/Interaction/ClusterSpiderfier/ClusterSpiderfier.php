<?php
/**
 * @file
 * Interaction: ClusterSpiderfier.
 */

namespace Drupal\openlayers_clusterspiderfier\Plugin\Interaction\ClusterSpiderfier;
use Drupal\openlayers\Component\Annotation\OpenlayersPlugin;
use Drupal\openlayers\Openlayers;
use Drupal\openlayers\Types\Interaction;

/**
 * Class ClusterSpiderfier.
 *
 * @OpenlayersPlugin(
 *  id = "ClusterSpiderfier",
 *  description = "Allows to access each feature of a cluster."
 * )
 *
 * @package Drupal\openlayers\Openlayers\Interaction\ClusterSpiderfier
 */
class ClusterSpiderfier extends Interaction {

  /**
   * {@inheritdoc}
   */
  public function optionsForm(array &$form, array &$form_state) {
    $form['options']['style'] = array(
      '#type' => 'select',
      '#title' => t('Style'),
      '#empty_option' => t('- Select a Style -'),
      '#default_value' => $this->getOption('style', ''),
      '#description' => t('Select the Style.'),
      '#options' => Openlayers::loadAllAsOptions('Style'),
    );

    $form['options']['radius'] = array(
      '#type' => 'textfield',
      '#title' => t('Radius'),
      '#default_value' => $this->getOption('radius', 50),
      '#description' => t('Freatures radius.'),
    );

    $form['options']['displayGeometry'] = array(
      '#type' => 'select',
      '#title' => t('Geometry'),
      '#default_value' => $this->getOption('displayGeometry', 'circle'),
      '#description' => t('Select the open geometry.'),
      '#options' => array(
        'circle' => t('Circle'),
        'spiral' => t('Spiral'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function optionsToObjects() {
    $import = parent::optionsToObjects();

    if ($style = $this->getOption('style')) {
      $import = array_merge($import, Openlayers::load('style', $style)->getCollection()->getFlatList());
    }

    return $import;
  }

  /**
   * {@inheritdoc}
   */
  public function attached() {
    parent::attached();
    $this->attached['libraries_load']['OL3ClusterSpiderfier'] = array('OL3ClusterSpiderfier');
    return $this->attached;
  }
}
